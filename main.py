from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///testDatabase"
db = SQLAlchemy(app)


class CarTable(db.Model):
    car = db.Column(db.String(250), primary_key=True)
    color = db.Column(db.String(250), )
    manufactured = db.Column(db.String(250), )

    db.create_all()


db.create_all()


@app.route('/car/all', methods=['GET'])
def get_all_cars():
    cars_dictionary = []

    all_cars = db.session.query(CarTable).all()
    for car in all_cars:
        temp = {"car": car.car,
                "color": car.color,
                "manufactured": car.manufactured, }
        cars_dictionary.append(temp)

    return jsonify(cars_dictionary)


@app.route('/api/', methods=['POST'])
def post_car_data():
    model = request.args.get('car')
    color = request.args.get('color')
    manufactured = request.args.get('manufactured')
    new_car = CarTable(car=model, color=color, manufactured=manufactured)
    db.session.add(new_car)
    db.session.commit()
    return "Data stored successfully"


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


if __name__ == "__main__":
    app.run(debug=True)
